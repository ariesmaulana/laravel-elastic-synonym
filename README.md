# instalasi

Pull repo ini

```
git clone https://gitlab.com/ariesmaulana/laravel-elastic-synonym.git
```

Ubah .env sesuai kebutuhan, salin dari .env.example
```
cp .env.example .env
```

Salin `docker-compose.yml.example` menjadi `docker-compose.yml`

```
cp docker-compose.yml.example docker-compose.yml
```

Build docker

```
docker-compose up --build -d
```
Jika berhasil silahkan kunjungi `localhost`

# Penggunaan

Untuk menjlankan perintah terkait seperti migrate, scout, dan sebagainya lakukan di dalam container app. Untuk masuk ke dalam container gunakan perintah.

```
docker exec -it container_app_name bash
```