<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title'=>'Laptop Series T','slug'=>'laptop-series-t','description'=>'Laptop series t'],
            ['title'=>'Laptop Series X','slug'=>'laptop-series-x','description'=>'Laptop series x'],
            ['title'=>'Notebook Series T','slug'=>'notebook-series-t','description'=>'Notebook series t'],
            ['title'=>'Notebook Series x','slug'=>'notebook-series-x','description'=>'Notebook series x'],
            ['title'=>'PC','slug'=>'pc','description'=>'daily pc'],
            ['title'=>'PC Gaming','slug'=>'pc-gaming','description'=>'pc gaming']
        ];
        Product::insert($data);
    }
}
