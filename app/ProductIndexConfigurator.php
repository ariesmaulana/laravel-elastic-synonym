<?php

namespace App;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class ProductIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    /**
     * @var array
     */
    protected $settings = [
        'analysis'=>[
            'filter' => [
                'product_synonym_filter' => [
                    'type'=> 'synonym',
                    'synonyms' => [
                        'laptop, notebook'
                    ]
                ]
            ],
            'analyzer' => [
                'product_synonyms' => [
                    'tokenizer' => 'standard',
                    'filter'=> [
                        'lowercase',
                        'product_synonym_filter'
                    ]
                ]
            ]
        ],
        
    ];
}