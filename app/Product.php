<?php

namespace App;

use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Searchable;
    protected $guarded = ['id'];
    /**
     * @var string
     */
    protected $indexConfigurator = ProductIndexConfigurator::class;

    /**
     * @var array
     */
    protected $searchRules = [
        //
    ];

    /**
     * @var array
     */
    protected $mapping = [
        'properties' => [
            'title' => [
                'type' => 'text',
                'analyzer'=>'product_synonyms'
            ],
            'slug' => [
                'type' => 'text',
                'analyzer'=>'standard'
            ],
            'description' => [
                'type' => 'text',
                'analyzer'=>'standard'
            ],
            'created_at' => [
                'type' => 'date',
                'format'=>'yyyy-MM-dd HH:mm:ss'
            ],
        ]
    ];
}