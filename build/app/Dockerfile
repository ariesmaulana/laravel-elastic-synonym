FROM php:7.1-fpm

RUN apt-get -y update --fix-missing
RUN apt-get upgrade -y

# add non root user
ARG PUID=1000
ENV PUID ${PUID}
ARG PGID=1000
ENV PGID ${PGID}

ARG WORKSPACE_GROUP=workspace
ENV WORKSPACE_GROUP ${WORKSPACE_GROUP}

ARG WORKSPACE_USER=dockuser
ENV WORKSPACE_USER ${WORKSPACE_USER}

RUN groupadd -g ${PGID} ${WORKSPACE_GROUP} && \
    useradd -u ${PUID} -g ${WORKSPACE_GROUP} -m ${WORKSPACE_USER} && \
    usermod -p "*" ${WORKSPACE_USER}

# Install useful tools
RUN apt-get -y install nano

# Install important libraries
RUN apt-get -y install --fix-missing apt-utils build-essential git curl libcurl3 libcurl3-dev zip libxml2-dev

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install node js
RUN apt-get -y install gnupg software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs

# Install xdebug
RUN pecl install xdebug-2.5.0
RUN docker-php-ext-enable xdebug

# Other PHP7 Extensions

RUN apt-get -y install libmcrypt-dev
RUN docker-php-ext-install mcrypt
RUN docker-php-ext-enable mcrypt


RUN apt-get -y install libsqlite3-dev libsqlite3-0 mysql-client
RUN docker-php-ext-install pdo_mysql 
RUN docker-php-ext-install pdo_sqlite
RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable pdo_mysql pdo_sqlite mysqli

RUN docker-php-ext-install curl
RUN docker-php-ext-install tokenizer
RUN docker-php-ext-install json
RUN docker-php-ext-enable curl tokenizer json

RUN apt-get -y install zlib1g-dev
RUN docker-php-ext-install zip
RUN docker-php-ext-enable zip

RUN apt-get -y install libicu-dev
RUN docker-php-ext-install -j$(nproc) intl
RUN docker-php-ext-enable intl

RUN docker-php-ext-install mbstring
RUN docker-php-ext-enable mbstring

RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ 
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-enable gd

RUN docker-php-ext-install exif
RUN docker-php-ext-enable exif

RUN apt-get remove --purge -y software-properties-common && \
apt-get autoremove -y && \
apt-get clean && \
apt-get autoclean

WORKDIR /var/www/

USER ${WORKSPACE_USER}